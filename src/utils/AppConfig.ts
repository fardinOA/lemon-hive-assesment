export const AppConfig = {
  site_name: 'Task',
  title: 'Task',
  description: 'This is a e-learning base project',
  locale: 'en',
};

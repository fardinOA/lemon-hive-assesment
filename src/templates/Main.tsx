import type { ReactNode } from 'react';

import { Header } from '@/component/compound';

// import { AppConfig } from '@/utils/AppConfig';

type IMainProps = {
  meta: ReactNode;
  children: ReactNode;
};
const Main = (props: IMainProps) => (
  // this is the menu items list

  <div>
    {/* meta data part */}
    {props.meta}

    {/* header part  main wrapper */}
    <Header />

    {/* children part or body part */}
    <div className={` dark:text-slate-400 h-screen dark:bg-black`}>
      <div>{props.children}</div>
    </div>
  </div>
);

export { Main };

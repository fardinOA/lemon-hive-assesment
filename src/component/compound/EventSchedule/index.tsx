import React from 'react';

import type { EventSchedulePropsType } from '../../../types/compound/EventSchedule';

import Schedule from '../../_root/Schedule';

const index = ({
  desc,
  time,
  colorVariant,
  path,
  date = 'Not Available',
}: EventSchedulePropsType) => {
  return (
    // main wrapper (not a active wrapper)
    <>
      {/* date wrapper */}
      <div className={`flex-center`}>
        <p
          className={`font-Inter_Regular text-xs font-bold text-blue-variantThree lg:text-base`}
        >
          {date}
        </p>
      </div>
      <div>
        <Schedule
          desc={desc}
          time={time}
          colorVariant={colorVariant}
          path={path}
        />
      </div>
    </>
  );
};

export default index;

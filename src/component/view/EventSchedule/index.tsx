import React from 'react';

import { Heading } from '@/component/_root';
import EventSchedule from '@/component/compound/EventSchedule';

const index = () => {
  const day = [
    {
      id: 6,
      name: 'Sat',
    },
    {
      id: 1,
      name: 'Mon',
    },
    {
      id: 2,
      name: 'Tue',
    },
    {
      id: 3,
      name: 'Wed',
    },
    {
      id: 4,
      name: 'Thu',
    },
    {
      id: 5,
      name: 'Fri',
    },

    {
      id: 0,
      name: 'Sun',
    },
  ];

  const row = [
    {
      time: '05:00',
      eventData: [
        {
          day: 1,
          event: [
            {
              date: '2022-09-12',
              title: 'Monday – workshops',
              start: '05:00',
              end: '06:00',
              eventId: 'react-finland-2022',
            },
          ],
        },
        {
          day: 2,
          event: [
            {
              date: '2022-09-13',
              title: 'Tuesday – workshops',
              start: '05:00',
              end: '06:00',
              eventId: 'react-finland-2022',
            },
          ],
        },
        {
          day: 3,
          event: [
            {
              date: '2022-09-13',
              title: 'Tuesday – workshops',
              start: '05:00',
              end: '06:00',
              eventId: 'react-finland-2022',
            },
          ],
        },
        {
          day: 4,
          event: [
            {
              date: '2022-09-15',
              title: 'Thursday – conference',
              start: '05:00',
              end: '05.50',
              eventId: 'React Finland 2022',
            },
          ],
        },
        {
          day: 5,
          event: [
            {
              date: '2022-09-16',
              title: 'Friday – conference',
              start: '05:00',
              end: '05.50',
              eventId: 'react-finland-2022',
            },
          ],
        },
      ],
    },
    {
      time: '06:00',
      eventData: [
        {
          day: 1,
          event: [
            {
              date: '2022-09-12',
              title: 'Monday – workshops',
              start: '06:00',
              end: '08:00',
              eventId: 'react-finland-2022',
            },
          ],
        },
        {
          day: 2,
          event: [
            {
              date: '2022-09-12',
              title: 'Monday – workshops',
              start: '06:00',
              end: '08:00',
              eventId: 'react-finland-2022',
            },
          ],
        },
      ],
    },
  ];
  return (
    <div className="flex items-center justify-center">
      <div className={`container mt-[12rem] space-y-[3%]  `}>
        <div className={` mx-4 p-3`}>
          <Heading
            label="Event Schedule"
            variant="h3"
            styleClass={`!text-blue-variantThree !text-[24px] lg:!text-[48px] !font-bold  font-Inter_Bold `}
          />
          <p
            className={`text-left font-Inter_Regular text-base font-normal !text-blue-variantOne lg:text-xl `}
          >
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis fuga
            error cupiditate cum nihil quasi mollitia alias maiores veritatis
            labore.
          </p>
        </div>

        <div className={` mx-2`}>
          <div className={` p-4`}>
            <div>
              <div className={`overflow-x-auto`}>
                <table
                  className={` table border-collapse border border-grey-light xl:table-fixed `}
                >
                  {/* table header */}
                  <thead>
                    <tr>
                      <td
                        scope="col"
                        className={`item min-w-[150px] border border-grey-dark  p-2 text-center font-Inter_Regular text-base font-bold lg:text-xl`}
                      >
                        Time
                      </td>
                      {day.map((day) => (
                        <React.Fragment key={day.id}>
                          <td
                            scope="col"
                            className={`item text-capitalize min-w-[150px] border  border-grey-dark p-2 text-center font-Inter_Regular text-base font-bold lg:text-xl`}
                          >
                            {day.name}
                          </td>
                        </React.Fragment>
                      ))}
                    </tr>
                  </thead>

                  {/* table content */}
                  <tbody>
                    {row.map((rootContainer, timeIndex) => {
                      return (
                        <tr key={timeIndex}>
                          <td
                            className={`item time-td min-w-[150px] space-y-8 border border-grey-dark p-2 text-center font-Inter_Regular text-base font-bold lg:text-xl`}
                          >
                            {rootContainer.time}
                          </td>

                          {day.map((rootDay, ind) => {
                            const allEvent = rootContainer.eventData.filter(
                              (messedUpEvent) =>
                                messedUpEvent.day === rootDay.id
                            );
                            if (allEvent.length) {
                              if (row.length - 1 === timeIndex) {
                                return (
                                  <td
                                    key={ind}
                                    className={`item min-w-[150px] space-y-4 border border-grey-dark p-2`}
                                  >
                                    {allEvent.map((singleEvent, ind) => {
                                      return (
                                        <React.Fragment key={ind}>
                                          {singleEvent.event.map(
                                            (finalData, ind) => {
                                              return (
                                                <EventSchedule
                                                  colorVariant="v2"
                                                  desc={finalData.title}
                                                  time={`${finalData.start} - ${finalData.end}`}
                                                  path={`/`}
                                                  date={finalData.date}
                                                  key={ind}
                                                />
                                              );
                                            }
                                          )}
                                        </React.Fragment>
                                      );
                                    })}
                                  </td>
                                );
                              }

                              return (
                                <td
                                  className={`item min-w-[150px] space-y-4 border border-grey-dark p-2`}
                                >
                                  {allEvent.map((singleEvent, ind) => {
                                    return (
                                      <React.Fragment key={ind}>
                                        {singleEvent.event.map(
                                          (finalData, ind) => {
                                            return (
                                              <EventSchedule
                                                colorVariant="v1"
                                                desc={finalData.title}
                                                time={`${finalData.start} - ${finalData.end}`}
                                                path={`/`}
                                                date={finalData.date}
                                                key={ind}
                                              />
                                            );
                                          }
                                        )}
                                      </React.Fragment>
                                    );
                                  })}
                                </td>
                              );
                            }

                            return (
                              <td
                                className={`item min-w-[150px] space-y-4 border border-grey-dark p-2`}
                              ></td>
                            );
                          })}
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default index;

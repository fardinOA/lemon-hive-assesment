import type { NextPage } from 'next';
import React from 'react';

import type { HeadingProps } from '../../../types/_root/index';

const Heading: NextPage<HeadingProps> = (props) => {
  const { variant, label, styleClass } = props;

  switch (variant) {
    case 'h1':
    case 'H1':
      return (
        <h1
          className={styleClass}
          style={{ fontSize: '140px', fontWeight: '700' }}
        >
          {label}
        </h1> //set the value of head one
      );
    case 'h2':
    case 'H2':
      return (
        <h2
          className={styleClass}
          style={{ fontSize: '48px', fontWeight: '700' }}
        >
          {label}
        </h2> //set the value of head two
      );
    case 'h3':
    case 'H3':
      return (
        <h3
          className={styleClass}
          style={{ fontSize: '20px', fontWeight: '400' }}
        >
          {label}
        </h3>
      );
    case 'h4':
    case 'H4':
      return (
        <h4
          className={styleClass}
          style={{ fontSize: '16px', fontWeight: '400' }}
        >
          {label}
        </h4>
      );
    case 'h5':
    case 'H5':
      return (
        <h5
          className={styleClass}
          style={{ fontSize: '12px', fontWeight: '400' }}
        >
          {label}
        </h5>
      );
    case 'h6':
    case 'H6':
      return (
        <h6
          className={styleClass}
          style={{ fontSize: '10px', fontWeight: '300' }}
        >
          {label}
        </h6>
      );

    default:
      return <h1 className={styleClass}>{label}</h1>;
  }
};

export default Heading;

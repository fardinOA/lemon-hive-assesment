import  {EventSchedulePropsType } from '../../../types/compound/EventSchedule'
import Link from 'next/link'
import React from 'react'


const index = ({
  desc = "Hello world",
  time ,
  colorVariant = "v1",  
  path = "/"
}:EventSchedulePropsType) => {
  let linkTagClass = ""  
  let scheduleNameClass = "" 
  let timeNameClass = "" 
  

  switch (colorVariant) {
    case "v1": {
      linkTagClass = `info-card block text-center p-[16px_8px] bg-[#FFFCF6] rounded-[8px] border-[1px] border-solid border-[#FCB12A] transition-all duration-500 translate-y-[0] hover:translate-y-[-3px] `
      scheduleNameClass = `lg:text-base info-card-title text-[#725114] leading-[1.1875em] font-bold lg:!text-base !text:xs !font-Inter_Regular `
      timeNameClass = `lg:text-[12px] info-card-interval text-[10px] leading-[1.25em] text-[#8B6F3B] mt-[8px] text-[10px] lg:text-base !font-Inter_Regular`
      break
    } //this will show the yellow color variant

    case "v2": {
      linkTagClass = `info-card block text-center p-[16px_8px] bg-[#F9FAFD] rounded-[8px] border-[1px] border-solid border-[#111D5E] transition-all duration-500 translate-y-[0] hover:translate-y-[-3px] `
      scheduleNameClass = `lg:text-base info-card-title text-[#111D5E] leading-[1.1875em] font-bold lg:!text-base !text:xs !font-Inter_Regular`
      timeNameClass = `lg:text-[12px] info-card-interval text-[10px] leading-[1.25em] text-[#111D5E] mt-[8px] text-[10px] lg:text-base !font-Inter_Regular `
      break
    } //this will show the yellow color variant
    default: {
      linkTagClass = `info-card block text-left p-[16px_8px] bg-[#FFFCF6] rounded-[8px] border-[1px] border-solid border-[#FCB12A] transition-all duration-500 translate-y-[0] hover:translate-y-[-3px] `
      scheduleNameClass = `lg:text-base info-card-title  text-[#725114] leading-[1.1875em]`
      timeNameClass = `lg:text-[12px] info-card-interval text-[10px] leading-[1.25em] text-[#8B6F3B] mt-[8px]`
    }
  }
  return (
    <Link href = {path}>
       <a className = {`${linkTagClass} `}>
     
        <p className = {`${scheduleNameClass}`}>
            {desc || "Not Found"}
        </p>
        
        <p className = {`${timeNameClass}`}>
            {time || "Not Found"}
        </p>
    </a>
    </Link>
  )
}

export default index
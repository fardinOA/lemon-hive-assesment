  export type EventSchedulePropsType = {
  desc?: string
  time?: string
  colorVariant?: string
  path?: string
  date?: string
}